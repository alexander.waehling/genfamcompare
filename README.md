# GenFamCompare Project

## Overview
GenFamCompare is a bioinformatics project designed to facilitate comparative genomic and proteomic analysis. This project includes two main Nextflow pipelines: `dnabased.nf` and `structurebased.nf`, each tailored for specific types of analysis. The pipelines integrate cutting-edge tools in bioinformatics to analyze genomic sequences and protein structures, providing insights into gene functions, evolutionary relationships, and protein folding patterns.

## Pipelines

### dnabased.nf (DNA-Based Analysis)
#### Purpose
This pipeline focuses on DNA/RNA sequence analysis, gene annotation, and evolutionary study. It is ideal for understanding genetic sequences, identifying gene locations, and exploring evolutionary relationships.

#### Key Components
- Genome Annotation using Prokka.
- Gene extraction and sequence analysis.
- Phylogenetic tree construction for evolutionary studies.

#### Flags and Options
- `--genomes <path>`: Specify the path to the genome data files.
- `--gene_family <path>`: Indicate the path to the gene family data.
- `--sorter_path <path>`: Define the path to the sorter script.
- `--extract_path <path>`: Set the path to the gene extraction script.
- `--enable_conda <bool>`: Enable or disable the Conda environment (`true` or `false`).
- `--conda_env <path>`: Specify the path to the Conda environment file.
- `--help`: Display the help message with detailed usage information.

### structurebased.nf (Structure-Based Analysis)
#### Purpose
This pipeline emphasizes protein structure analysis. It extends genomic insights to the protein level, focusing on the three-dimensional conformation of proteins.

#### Key Components
- Gene extraction tailored for protein-coding genes.
- Protein folding prediction and analysis using tools like AlphaFold.
- Structural comparison and analysis.

#### Flags and Options
- `--proteome <path>`: Path to the proteome database.
- `--gene_fasta <path>`: Path to the gene_fasta files.
- `--alphafold_run_path <path>`: Path to the AlphaFold run script.
- `--foldseek_path <path>`: Path to the FoldSeek container.
- `--distance_matrix_path <path>`: Path to the distance matrix script.
- `--help`: Display the help message with detailed usage information.

## Key Tools

### Prokka
#### Description
Prokka is a software tool for annotating bacterial, archaeal, and viral genomes quickly and accurately. It identifies genomic features like CDS, rRNA, tRNA, and other functional elements, facilitating a comprehensive understanding of genetic material.

#### Usage in GenFamCompare
Prokka is used in the `dnabased.nf` pipeline for genome annotation.

### Foldseek
#### Description
Foldseek is a tool for fast and accurate protein structure searching and comparison. It allows for the identification of structural similarities and differences between proteins, aiding in functional annotation and evolutionary studies.

#### Usage in GenFamCompare
Foldseek is integrated into the workflow for structural analysis in the `structurebased.nf` pipeline.

### AlphaFold
#### Description
AlphaFold is an AI-driven tool developed by DeepMind for predicting protein structures. It has revolutionized the field of structural biology by providing highly accurate predictions of protein 3D structures from their amino acid sequences.

#### Usage in GenFamCompare
In the `structurebased.nf` pipeline, AlphaFold is used for predicting the structures of proteins coded by extracted genes, facilitating advanced structural and functional analysis.