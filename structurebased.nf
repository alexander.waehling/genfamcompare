#!/usr/bin/env nextflow

nextflow.enable.dsl=2

helpMessage = """
Usage: nextflow run structurebased.nf [options]

Options:
    --genomes PATH           Path to genomes data
    --gene_family PATH       Path to gene family data
    --sorter_path PATH       Path to sorter script
    --extract_path PATH      Path to extract script
    --alphafold_run_path PATH Path to AlphaFold run script
    --foldseek_path PATH     Path to FoldSeek container
    --distance_matrix_path PATH Path to distance matrix script
    --enable_conda BOOL      Whether to enable Conda environment (true/false)
    --conda_env PATH        Path to Conda environment file
    --help                  Print this help message
"""

// Check if help is requested
if (params.help) {
    println(helpMessage)
    exit 0
}

// The process ANNOTATE_GENOMES receives genome data and annotates it by using Prokka; also uses Conda environment defined in params.conda_env
process ANNOTATE_GENOMES {
    label 'slurm_medium'
    conda params.conda_env
    input:
        path genome_path // Path to the genome data file
    output:
        path "*.faa" // Export annotated genomes as .faa file format
    script:
        """
        data=\$(echo $genome_path | cut -d'.' -f1)
        prokka --outdir . --prefix \$data $genome_path --force --centre X --compliant
        """
}

// Process EXTRACT_GENE extracts a specific gene from the given proteome and gene_fasta databases using NCBI BLAST
process EXTRACT_GENE {
    label 'slurm_medium'
    input:
        path proteome // Path to the proteome database
        path gene_fasta // Path to the gene_fasta files
    output:
        path "*.fasta" // Export the extracted gene information as .fasta format
    script:
        """
        module load ncbi-blast
        outfile=${proteome}.out
        makeblastdb -in $gene_fasta -title reference -dbtype prot -out db
        blastp -query $proteome -db db -outfmt "6 qseqid evalue" | ${params.sorter_path} -k2 | head -n1 > \$outfile
        python ${params.extract_path} $proteome \$outfile > ${proteome}.fasta
        """
}

// Process FOLD_GENE runs AlphaFold to predict protein structures, uses GPU resources
process FOLD_GENE {
    label 'gpu'
    publishDir "./output", mode: 'copy' // Copy the output file to the directory ./output
    input:
        path gene // Path to the gene sequence
    output:
        path "*.pdb" // Export the protein structure in .pdb format
    script:
        """
        module load singularity
        name=\$(echo $gene | cut -d'.' -f1)
        bash ${params.alphafold_run_path} ${params.alphafold_config_path} --fasta_paths $gene --output_dir .
        cp \${name}.faa/ranked_0.pdb ./\${name}.pdb
        """
}

// Process BUILD_TREE builds a phylogenetic tree based on foldseek distance matrix calculation
process BUILD_TREE {
    label 'slurm_medium'
    conda params.conda_env
    publishDir "./output", mode: 'copy' // Copy the output file to the directory ./output
    stageInMode 'copy'
    input:
        path genefamily // Path to the gene families information
    output:
        path "*.tree" // Export the phylogenetic tree as .tree format
    script:
        """
        module load singularity
        python ${params.distance_matrix_path} -foldseek_path ${params.foldseek_path} -input_path .
        """
}

// Combining all processes in a single workflow 
workflow {
    genomes = channel.fromPath(params.genomes) // Read genomes from user-defined path
    gene_family = params.gene_family // Get the gene family from parameters

    ANNOTATE_GENOMES(genomes) // Run ANNOTATE_GENOMES process with genome as input
    EXTRACT_GENE(ANNOTATE_GENOMES.out, gene_family) // Run EXTRACT_GENE process on the output of ANNOTATE_GENOMES and gene_family
    FOLD_GENE(EXTRACT_GENE.out) // Run FOLD_GENE process on the output of EXTRACT_GENE
    BUILD_TREE(FOLD_GENE.out.collect()) // Run BUILD_TREE process to generate a phylogenetic tree based on the output of FOLD_GENE
}
