
nextflow.enable.dsl=2

process ANNOTATE_GENOMES {
    label 'slurm_medium'
    conda "/usr/users/awaehli/.conda/envs/phylogenomics"
    input:
        path genome_path

    output:
        path "*.faa"
    script:
        """
        data=\$(echo $genome_path | cut -d'.' -f1)
        prokka --outdir . --prefix \$data $genome_path --force --centre X --compliant
        """
}

process EXTRACT_GENE{
    label 'slurm_medium'
    input:
        path proteome
        path gene_fasta
        path extract_path
        path scisort
    output:
        path "*.fasta"

    script:
    """
    module load ncbi-blast
    prot='$proteome'
    outfile=\${prot}.out
    makeblastdb -in $gene_fasta -title reference -dbtype prot -out db
    blastp -query $proteome -db db -outfmt "6 qseqid evalue" | ./$scisort -k2 | head -n1 > \$outfile
    python $extract_path $proteome \$outfile > $proteome\.fasta
    """
}

process BUNDLE_GENES {
    label 'local'
    publishDir "./output", mode: 'copy'
    input:
        path genes 
    output:
        path "*.fasta"
    """
    cat $genes > genes.fasta
    """
}


process BUILD_TREE {
    label 'slurm_medium'
    conda "/usr/users/awaehli/.conda/envs/phylogenomics"
    publishDir "./output", mode: 'copy'

    input:
        path genefamily
    output:
        path "*.treefile"
    script:
        """
        module load mafft
        mafft --localpair --maxiterate 1000 $genefamily > genes_aligned.fasta
        iqtree -s genes_aligned.fasta --seqtype AA -B 1000 -T 8 -m MFP --keep-ident
        """
}

workflow {
    root_dir = '/scratch/users/awaehli/Nextflow/'
    genomes = channel.fromPath"$root_dir/genomes/*.fas"
    gene_family = "$root_dir/reference.fasta"
    sorter = "/$root_dir/GeneFamCompare/assets/scisort"
    extract = "$root_dir/GeneFamCompare/assets/extract.py"
    ANNOTATE_GENOMES(genomes)
    EXTRACT_GENE(ANNOTATE_GENOMES.out, gene_family, extract, sorter)
    BUNDLE_GENES(EXTRACT_GENE.out.collect())
    BUILD_TREE(BUNDLE_GENES.out)
}