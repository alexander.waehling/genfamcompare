from asyncio.subprocess import DEVNULL
from turtle import distance
import numpy as np
import subprocess
import os
import shutil
import argparse
from skbio import DistanceMatrix
from skbio.tree import nj
from os import listdir
from os.path import isfile, join

# foldseek_path = "/scratch/projects/alphafold/container/foldseek.sif"
# input_path = "."


def calculate_distance(score_ii, score_jj, score_ij):
    """
    Calculates the distance between two structures using the bit scores for each structure and their pairwise bit score.

    Args:
        score_ii (float): Bit score for structure i.
        score_jj (float): Bit score for structure j.
        score_ij (float): Pairwise bit score between structures i and j.

    Returns:
        float: Distance between structures i and j.
    """
    return -1 * (np.log10(2*score_ij/(score_ii+score_jj)))

def calculate_matrix(bitscore_matrix, length):
    """
    Calculates the distance matrix using the bit score matrix.

    Args:
        bitscore_matrix (numpy.ndarray): Bit score matrix for all pairs of structures.
        length (int): Number of structures.

    Returns:
        numpy.ndarray: Distance matrix for all pairs of structures.
    """
    distance_matrix = np.zeros((length, length))
    for i in range(length):
        for j in range(length):
            distance_matrix[i,j] = calculate_distance(bitscore_matrix[i,i], bitscore_matrix[j,j], bitscore_matrix[i,j])
    return distance_matrix

def parse_foldseek_output(out_file):
    """
    Parses Foldseek output file and returns a dictionary of bit scores.

    Args:
        out_file (file): File object containing Foldseek output.

    Returns:
        dict: Dictionary of bit scores.
    """
    score_dict = {}
    for line in out_file.readlines():
        bitscore = int(line.split("\t")[1])
        query = line.split("\t")[0]
        score_dict[query] = bitscore
    return score_dict

def main():
    """
    Main function that generates a phylogenetic tree from a set of protein structures using Foldseek and skbio.
    """
    
    # Initialize command line argument parser
    parser = argparse.ArgumentParser()
    
    # Add command line arguments for foldseek and input path
    parser.add_argument(
        '-foldseek_path', 
        type=str, 
        help='Path to foldseek container', 
        required=True
    )
    parser.add_argument(
        '-input_path', 
        type=str, 
        help='Directory containing input files',
        required=True
    )
    
    # Parse command line arguments
    args = parser.parse_args()
    
    # Assign command line arguments to variables
    foldseek_path = args.foldseek_path
    input_path = args.input_path
    
    # Define paths for database, temporary matrix and temporary files
    db_path = f"{input_path}/db"
    mat_tmp_path = f"{input_path}/mat_tmp"
    tmp_path = f"{input_path}/tmp"

    # Get a list of PDB files in the input directory
    pdb_files = [f for f in listdir(input_path) if isfile(join(input_path, f)) and f.endswith(".pdb")]

    # Create required directories
    subprocess.run(["mkdir", "-p", db_path])
    subprocess.run(["mkdir", "-p", mat_tmp_path])
    
    # Initialize variables for matrix and score dictionaries
    fold_number = len(pdb_files)
    bitscore_mat = np.zeros((fold_number, fold_number))
    all_score_dict = {}
    
    # Loop through PDB files and calculate scores
    for i, i_file in enumerate(pdb_files):
        # Relevant file paths and names
        pdb_file = i_file.split('/')[-1]
        database = f"db/{i_file}"
        # Create a database for each PDB file
        subprocess.run(["singularity", "run", "-B", f"{input_path}:/mnt/",foldseek_path, "createdb", f"/mnt/{pdb_file}", f"/mnt/{database}"], capture_output=False)

        # Perform foldseek search for each pair of PDB files
        fold1 = i_file.split('.')[0].split('/')[-1]
        outfile = f"{fold1}_foldseek.txt"
        subprocess.run(["singularity", "run","-B", f"{input_path}:/mnt",foldseek_path, "easy-search", f"/mnt/", f"/mnt/db/{pdb_file}", f"/mnt/mat_tmp/{outfile}", "/mnt/tmp", "--format-output", "query,bits"], capture_output=True)

        # Parse foldseek output and store scores in dictionaries
        score_dict = {}
        with open(f"{mat_tmp_path}/{outfile}", "r") as f:
            score_dict = parse_foldseek_output(f)
            all_score_dict[i_file] = score_dict
        
        # Remove temporary output files
        os.remove(f"{mat_tmp_path}/{outfile}")
        print(f"{i_file}: done")
    
    # Fill the bitscore matrix with scores from dictionaries
    for i, base in enumerate(pdb_files):
        for j, query in enumerate(pdb_files):
            bitscore_mat[i,j] = all_score_dict[base][query] 
    
    # Remove temporary directories
    shutil.rmtree(db_path, ignore_errors=True)
    shutil.rmtree(mat_tmp_path, ignore_errors=True)
    shutil.rmtree(tmp_path, ignore_errors=True)

    # Calculate distance matrix and create phylogenetic tree using skbio
    distance_matrix = np.abs(calculate_matrix(bitscore_mat, fold_number))
    distance_matrix = np.tril(distance_matrix) + np.tril(distance_matrix, -1).T
    dm = DistanceMatrix(distance_matrix, [i.split(".")[0] for i in pdb_files])
    tree = nj(dm)
    
    # Print and save the phylogenetic tree
    print(tree.ascii_art())
    newick_str = nj(dm, result_constructor=str)
    with open("newick_tree.tree", "w") as f:
        f.write(newick_str)

# Call the main function
main()
