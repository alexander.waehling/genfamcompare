# Import required modules
import os
import sys
from Bio import SeqIO
from os import listdir
from os.path import isfile, join

# Set the threshold for filtering blast results
THRESHOLD = 10e-10

# Check if the correct number of arguments is provided
if (len(sys.argv) < 3):
    raise ValueError("Please provide a genome and a blastfile")

# Get the paths to the genome and blast result files from command line arguments
genome_path = sys.argv[1]
blastout_path = sys.argv[2]

# Check if the blast result file is empty, if so, exit the program
if(os.path.getsize(blastout_path) == 0):
    sys.exit(0)

# Open the blast result file for reading
blastout = open(blastout_path, 'r')

# Open the genome file for reading
genome = open(genome_path, 'r')

# Read the first line of the blast result and extract the e-value
content = blastout.readline().split('\t')
evalue = float(content[1])

# Check if the e-value is below the threshold
if(evalue < THRESHOLD):
    # If below the threshold, retrieve the gene ID from the blast result
    gene_id = content[0]

    # Extract the name of the genome file without the extension and path
    name = genome_path.split('.')[0].split('/')[-1]

    # Create an index for fast access to sequences in the genome file
    gene_dict = SeqIO.index(genome_path, "fasta")

    # Retrieve the sequence of the gene using the gene ID
    gene = gene_dict[gene_id]

    # Rename the gene ID to the genome file name
    gene.id = name

    # Print the gene in FASTA format (header and sequence)
    print(f'>{gene.id}')
    print(gene.seq)