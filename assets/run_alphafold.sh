#!/bin/bash
module load singularity
# source ../alphafold_config.sh
# export ALPHAFOLD_IMAGE="/scratch/projects/alphafold/container/alphafold.sif"
# export TMP_DIR="/scratch/projects/alphafold/tmp/:/mnt"
# export DOWNLOAD_DIR='/scratch/projects/alphafold/data'
# ROOT_MOUNT_DIRECTORY='/scratch/projects/alphafold/mnt'
# export TMP_DIR="$TMP_LOCAL:/mnt"

config_file=$1
shift
# echo $config_file
# Check if the config file exists
if [ ! -f "$config_file" ]; then
    echo "Configuration file not found: $config_file"
    exit 1
fi

# Read the config file line by line
while IFS='=' read -r key value; do
    case $key in
        alphafold_image)
            ALPHAFOLD_IMAGE=$value
            ;;
        tmp_dir)
            TMP_DIR=$value
            ;;
        download_dir)
            DOWNLOAD_DIR=$value
            ;;
        root_mount_dir)
            ROOT_MOUNT_DIRECTORY=$value
            ;;
        # Add more keys as needed
        *)
            # You can handle unknown keys here
            ;;
    esac
done < "$config_file"

echo $ALPHAFOLD_IMAGE

create_mount ()
{
  mount_name=$1
  path=`readlink -f $2`
  source_path=`dirname $path`
  target_path=$ROOT_MOUNT_DIRECTORY/$mount_name
  echo "Mounting $source_path -> $target_path" 1>&2
  [ -f "bindings.txt" ] && . bindings.txt
  if [ -z "$SINGULARITY_BIND" ]
  then
    export SINGULARITY_BIND="$source_path:$target_path"
  else
    export SINGULARITY_BIND="${SINGULARITY_BIND},$source_path:$target_path"
  fi
  echo "export SINGULARITY_BIND=\"$SINGULARITY_BIND\"" > bindings.txt
  echo "$target_path/`basename $path`"
}

# rm bindings.txt

# output_dir=$PWD

model_names='model_1,model_2,model_3,model_4,model_5'
multimer=false
myargs=""
while :; do
  case $1 in
    --fasta_paths)       # Takes an option argument; ensure it has been specified.
      i=0
      fasta=""
      for FASTA_PATH in ${2//,/ }
      do
        target_fasta_path=$(create_mount "fasta_path_${i}" "$FASTA_PATH")
        if [ -z "$fasta" ]
        then
          fasta="$target_fasta_path"
        else
          fasta="$fasta,$target_fasta_path"
        fi
        let i=i+1
      done
      myargs="$myargs --fasta_paths=$fasta"
    
      shift
      ;;
    --max_template_date)       # Takes an option argument; ensure it has been specified.
      max_template_date=$2
      shift
      ;;
    --model_preset)       # Takes an option argument; ensure it has been specified.
      model_preset=$2
      myargs="$myargs --model_preset=$model_preset"
      if [ $model_preset = "multimer" ]
      then
        multimer=true
      fi
      shift
      ;;
    --output_dir)       # Takes an option argument; ensure it has been specified.
      OUTDIR=`create_mount outdir $2`
      shift
      ;;
    --)              # End of all options.
      shift
      break
      ;;
    -?*)
      printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
      ;;
    *)               # Default case: No more options, so break out of the loop.
      break
  esac
 
  shift
done



data_dir=$DOWNLOAD_DIR
myargs="$myargs --data_dir=`create_mount data_dir $data_dir`"

uniref90_database_path="$DOWNLOAD_DIR/uniref90/uniref90.fasta"
myargs="$myargs --uniref90_database_path=`create_mount uniref90_database_path $uniref90_database_path`"

# Path to the MGnify database for use by JackHMMER.
mgnify_database_path="$DOWNLOAD_DIR/mgnify/mgy_clusters_2018_12.fa"
myargs="$myargs --mgnify_database_path=`create_mount mgnify_database_path $mgnify_database_path`"

# Path to the BFD database for use by HHblits.
bfd_database_path="$DOWNLOAD_DIR/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt"
myargs="$myargs --bfd_database_path=`create_mount bfd_database_path $bfd_database_path`"

# Path to the Small BFD database for use by JackHMMER.
#small_bfd_database_path="$DOWNLOAD_DIR/small_bfd/bfd-first_non_consensus_sequences.fasta"
#myargs="$myargs --bfd_database_path=`create_mount bfd_database_path $bfd_database_path`"

# Path to the Uniclust30 database for use by HHblits.
uniclust30_database_path="$DOWNLOAD_DIR/uniclust30/uniclust30_2018_08/uniclust30_2018_08"
myargs="$myargs --uniclust30_database_path=`create_mount uniclust30_database_path $uniclust30_database_path`"



# Path to the PDB70 database for use by HHsearch.
if [ $multimer = false ]
then 
  pdb70_database_path="$DOWNLOAD_DIR/pdb70/pdb70"
  myargs="$myargs --pdb70_database_path=`create_mount pdb70_database_path $pdb70_database_path`"
else
  uniprot_database_path="$DOWNLOAD_DIR/uniprot/uniprot.fasta"
  myargs="$myargs --uniprot_database_path=`create_mount uniprot_database_path $uniprot_database_path`"
  
  pdb_seqres_database_path="$DOWNLOAD_DIR/pdb_seqres/pdb_seqres.txt"
  myargs="$myargs --pdb_seqres_database_path=`create_mount pdb_seqres_database_path $pdb_seqres_database_path`"
fi

# Path to a directory with template mmCIF structures, each named <pdb_id>.cif')
template_mmcif_dir="$DOWNLOAD_DIR/pdb_mmcif/mmcif_files"
myargs="$myargs --template_mmcif_dir=`create_mount template_mmcif_dir $template_mmcif_dir`"

# Path to a file mapping obsolete PDB IDs to their replacements.
obsolete_pdbs_path="$DOWNLOAD_DIR/pdb_mmcif/obsolete.dat"
myargs="$myargs --obsolete_pdbs_path=`create_mount obsolete_pdbs_path $obsolete_pdbs_path`"

# OUTDIR=$1
echo $OUTDIR
. bindings.txt
# Create output
# export SINGULARITY_BIND=$SINGULARITY_BIND,${output_dir}:${ROOT_MOUNT_DIRECTORY}/output

command_args="$myargs --output_dir=$OUTDIR --max_template_date=2021-05-14 --logtostderr --use_gpu_relax=true"

export TF_FORCE_UNIFIED_MEMORY=1
export XLA_PYTHON_CLIENT_MEM_FRACTION=4.0

# echo $command_args
# echo $SINGULARITY_BIND
singularity run -B $TMP_DIR --nv $ALPHAFOLD_IMAGE $command_args 
